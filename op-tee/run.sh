# Mount the shared folder
mkdir -p /mnt/host
mount -t 9p -o trans=virtio host /mnt/host



# Invoking the example usage command
LD_LIBRARY_PATH=/mnt/host/optee_client/out/libteec/ AFL_POST_LIBRARY=/mnt/host/afl-tee/out/afl_validate.so /mnt/host/afl/afl-fuzz -i /mnt/host/afl/seeds -t 300+ -o /tmp/state -M $1 -- /mnt/host/afl-tee/ca/tee_invoke_svc


