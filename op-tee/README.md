The first thing to make sure is that you have the latest version of repo, as older versions might not work.
Follow the instructions from: https://source.android.com/setup/build/downloading#installing-repo

It is necessary to clone the `3.3.0` branch.
`python repo init -u https://github.com/OP-TEE/manifest.git -b 3.3.0`

If you wish to build the project with QEMU v8 for Aarch64 support, I have included the required modified manifest `qemu_v8.xml`, this is for QEMU v8 with the 3.3.0 versions of sources. The remote version does not have the correct version for the core OP-TEE repos and will not work.
Pass this local manifest file using `python repo init -m qemu_v8.xml`


Clone the `optee_fuzzer` project from https://github.com/Riscure/optee_fuzzer into the build directory. However it expects the directory name to be `afl-tee`, so you can run `cp -r optee_fuzzer afl-tee`.

Next, apply `build.diff` and `optee_client.diff` patches from `optee_fuzzer`, and use the `optee_os.diff` which I have provided and modified from the original.
Git apply may not work so it is necessary to manually apply the patches.
Inside the OP-TEE build directory: 

`patch -p1 < ../optee_os.diff` # Do not use the optee_os.diff from `optee_fuzzer`

`patch -p1 < ../optee_fuzzer/build.diff`

`patch -p1 < ../optee_fuzzer/optee_client.diff`


Finally we can build using:

```
cd build

make edk2 linux soc-term buildroot qemu
make run
```


I have also included `run.sh`, these include instructions to execute inside QEMU.
