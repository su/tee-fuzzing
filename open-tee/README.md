Open-TEE successfully builds on Ubuntu 18.04 using the autotools build path.

Follow the instructions here:

http://open-tee.github.io/documentation/#autotools


However when the makefiles have been generated after running `../autogen.sh`, it was necessary for me to change `-rdynamic` to `--export-dynamic` in all generated makefiles due to my compiler.
It also may be necessary to use `-export-dynamic` instead, using only one dash.

While Open-TEE works successfully on 18.04, there are issues running the engine on 20.10. 
Specifically the `tee_launcher` process is not created as expected in `emulator/opentee-main/main.c`

In order to successfully build on 20.10, it is necessary to utilize an older version of OpenSSL as well.
The following commands should help address these issues.
```
# Download openssl 1.0.2 sources, and replace includes

sudo mv /usr/include/openssl /usr/include/openssl_original
sudo mv openssl-1.0.2l/include/openssl /usr/include/openssl

sudo mv /usr/include/x86_64-linux-gnu/openssl/opensslconf.h /usr/include/x86_64-linux-gnu/openssl/opensslconf.h.original
sudo cp /usr/include/openssl/opensslconf.h /usr/include/x86_64-linux-gnu/openssl/opensslconf.h

# Strip leading dash in CA Makefile, after default generation using build instructions.
sed -i -- 's/\-\-export\-dynamic/\-export\-dynamic/g' Open-TEE/build/CAs/Makefile
```

